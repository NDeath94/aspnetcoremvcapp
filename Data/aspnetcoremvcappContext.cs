﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace aspnetcoremvcapp.Models
{
    public class aspnetcoremvcappContext : DbContext
    {
        public aspnetcoremvcappContext (DbContextOptions<aspnetcoremvcappContext> options)
            : base(options)
        {
        }

        public DbSet<aspnetcoremvcapp.Models.Movie> Movie { get; set; }
    }
}
